/*
 * Device Tree constants for MediaTek SoC's Ethernet switch
 *
 * Author: Daniel Golle <daniel@makrotopia.org>
 *
 * Copyright:   (C) 2017 Daniel Golle
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

#ifndef _DT_BINDINGS_MTK_SOC_ETH_H
#define _DT_BINDINGS_MTK_SOC_ETH_H

#define VLAN_CONFIG_LLLLL 0x3f
#define VLAN_CONFIG_LLLLW 0x2f
#define VLAN_CONFIG_WLLLL 0x3e

#endif
